package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
   
    int maxCapacity = 20;
    ArrayList<FridgeItem> items =new ArrayList<>();

    @Override
    public int nItemsInFridge(){
    return items.size();    
    }

    @Override
    public int totalSize(){
        return maxCapacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if(items.size()>= totalSize())
            return false;
        return items.add(item);
    }

    @Override
    public void takeOut(FridgeItem item) {
        if(!items.contains(item))
            throw new NoSuchElementException("Item not in fridge");
        items.remove(item);
    }
    
    @Override
    public void emptyFridge() {
    items.clear();
    
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem > ExpiredItems = new ArrayList<>();
       for(FridgeItem item: items){
           if(item.hasExpired()){
               ExpiredItems.add(item);
           }
       }
       items.removeAll(ExpiredItems);
       return ExpiredItems;
    }
        
    }